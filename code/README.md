Pair easy to navigate commands <br>

sudo docker-compose up , <br>
sudo docker exec -it  shipnotificationsystem_php_1 bash , <br>
cd /code ,<br>
composer update ,<br>
composer dump-autoload ,<br>

Make .env file  then ,<br>
php artisan key:generate ,<br>
php artisan migrate --seed ,<br>
php artisan storage:link ,<br>

Admin credentials:
 - email: admin@gmail.com
 - pass: 123456
 
User credentials:
 - email: user@gmail.com
 - pass: 123456
 