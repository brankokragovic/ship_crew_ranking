<?php

namespace Tests\Unit;


use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CrewTest extends TestCase
{

	use WithFaker , WithoutMiddleware;

	/**
	 * @test
	 *
	 * A create user test.
	 */
	public function createUser()
	{
		$data = [
			'name' => $this->faker->name,
			'surname' => $this->faker->lastName,
			'email' => $this->faker->email,
			'password' => $this->faker->password,
			'rank_id' => $this->faker->numberBetween(1,3),
			'ship_id' => $this->faker->numberBetween(1,2),
		];

		$response = $this->json('post', 'store/member',$data);

		$response->assertStatus(302);

		$response->assertRedirect('/');

	}

	/**
	 * @test
	 *
	 * A create notification test.
	 */
	public function createNotification(){

		$data = [
			'rank_id' => $this->faker->numberBetween(1,3),
			'notification' => $this->faker->text(100),
		];

		$response = $this->json('post', 'notify/member',$data);

		$response->assertRedirect('/');

	}

}

