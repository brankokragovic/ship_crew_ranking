<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class RankTest extends TestCase
{

	use WithFaker, WithoutMiddleware;


	/**
	 * @test
	 *
	 * A create rank test.
	 */

	public function createRank()
	{
		$data = [
			'name' => $this->faker->name
		];

		$response = $this->json('post', 'store/rank',$data);

		$response->assertStatus(302);

		$response->assertRedirect('/');

	}
}
