<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth.login');
});

Auth::routes();

Route::get('profile','Crew\\ProfileController@index');

Route::group(['middleware' => ['admin']], function () {

	Route::get('admin','Admin\\CrewController@index');
	Route::get('member/{id}','Admin\\CrewController@crewMember')->name('crewMember');
	Route::get('create/member','Admin\\CrewController@createMember')->name('createMember');
	Route::post('store/member','Admin\\CrewController@storeMember')->name('storeMember');

	Route::get('notify/member','Admin\\CrewController@createNotification')->name('createNotify');
	Route::post('notify/member','Admin\\CrewController@sendNotification')->name('memberNotify');
	Route::get('all/member','Admin\\CrewController@showAll')->name('showMembers');
    Route::post('update/member','Admin\\CrewController@updateMember')->name('updateMember');

	Route::get('create/ship','Admin\\ShipController@createShip')->name('createShip');
	Route::post('store/ship','Admin\\ShipController@storeShip')->name('storeShip');
	Route::get('all/ship','Admin\\ShipController@GetShips')->name('showShips');

	Route::get('create/rank','Admin\\RankController@createRank')->name('createRank');
	Route::post('store/rank','Admin\\RankController@storeRank')->name('storeRank');



});

