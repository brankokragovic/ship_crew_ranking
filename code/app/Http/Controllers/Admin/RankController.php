<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RankStoreRequest;
use App\Repositories\RankRepository;
use Illuminate\Http\Request;

class RankController extends Controller
{
	public $rankRepository;

	public function __construct(RankRepository $rankRepository)
	{
		$this->rankRepository = $rankRepository;
	}
	
	
	public function createRank(){

		return view('rank.create');
	}

	public function storeRank(RankStoreRequest $request){

		$this->rankRepository->store($request);

		return redirect()->back()->with('success','Rank created');
	}

}
