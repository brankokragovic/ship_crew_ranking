<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ShipStoreRequest;
use App\Repositories\CrewRepository;
use App\Repositories\RankRepository;
use App\Repositories\ShipRepository;
use Illuminate\Http\Request;

class ShipController extends Controller
{

	public $crewRepository;

	public $rankRepository;

	public $shipRepository;


	public function __construct(
		CrewRepository $crewRepository,
		ShipRepository $shipRepository,
		RankRepository $rankRepository
	) {
		$this->crewRepository = $crewRepository;

		$this->rankRepository = $rankRepository;

		$this->shipRepository = $shipRepository;
	}


	public function createShip()
	{
		return view('ship.create');
	}

	public function storeShip(ShipStoreRequest $request)
	{

		$this->shipRepository->store($request);

		return redirect()->back()->with('success', 'Ship created');
	}

	public function GetShips()
	{
		$ships = $this->shipRepository->all();

		return view('ship.all', compact('ships'));
	}


	public function getShip($id)
	{
		$ship = $this->shipRepository->getById($id);

		return view('ship.one', compact('ship'));
	}

}
