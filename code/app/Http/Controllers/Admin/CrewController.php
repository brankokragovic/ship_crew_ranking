<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CrewNotifyRequest;
use App\Http\Requests\CrewStoreRequest;
use App\Repositories\CrewRepository;
use App\Repositories\RankRepository;
use App\Repositories\ShipRepository;
use Illuminate\Http\Request;

class CrewController extends Controller
{
	public $crewRepository;

	public $rankRepository;

	public $shipRepository;


	public function __construct(
		CrewRepository $crewRepository,
		ShipRepository $shipRepository,
		RankRepository $rankRepository
	) {
		$this->crewRepository = $crewRepository;

		$this->rankRepository = $rankRepository;

		$this->shipRepository = $shipRepository;
	}

	public function index()
	{
		return view('admin-view');

	}

	public function crewMember($id)
	{
		$member = $this->crewRepository->getById($id);

		$ships = $this->shipRepository->all();

		$ranks = $this->rankRepository->all();

		return view('crew.member', compact('member', 'ships', 'ranks'));

	}

	public function createMember()
	{

		$ships = $this->shipRepository->all();

		$ranks = $this->rankRepository->all();

		return view('crew.create', compact('ranks', 'ships'));
	}


	public function storeMember(CrewStoreRequest $request)
	{
		$this->crewRepository->store($request);

		return redirect()->back()->with('success', 'Crew member created!');
	}

	public function createNotification()
	{

		$ranks = $this->rankRepository->all();

		return view('crew.notify', compact('ranks'));

	}


	public function editMember()
	{
		$ships = $this->shipRepository->all();

		$ranks = $this->rankRepository->all();

		return view('crew.member', compact('ranks', 'ships'));
	}

	public function updateMember(Request $request){

		$this->crewRepository->update($request);

		return redirect()->back()->with('success', 'Crew member updated!');

	}


	public function sendNotification(CrewNotifyRequest $request)
	{

		$this->crewRepository->notify($request);

		return redirect()->back()->with('success', 'Notification is send!');

	}

	public function showAll()
	{
		$data = $this->crewRepository->all();

		return view('crew.all', compact('data'));

	}
}
