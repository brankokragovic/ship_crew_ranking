<?php

namespace App\Http\Controllers\Crew;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
	public function index()
	{
		$user = User::where('id', auth()->user()->id)->with('rank')->with('ship')->first();

		$notifications = Notification::where('user_id', auth()->user()->id)->where('read', false)->get();

		return view('profile', compact('user', 'notifications'));
	}
}
