<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SendEmailService
{

	public function sendEmail($request)
	{
		$user = DB::table('users')->where('email', '=', $request->email)->first();

		if (count((array)$user) < 1) {
			return redirect()->back()->withErrors(['email' => trans('User does not exist')]);
		}

		DB::table('password_resets')->insert([
			'email' => $request->email,
			'token' => Str::random(64),
			'created_at' => Carbon::now()
		]);

		$tokenData = DB::table('password_resets')->where('email', $request->email)->first();

		if ($this->sendResetEmail($request->email, $tokenData->token)) {

			return redirect()->back()->with('status', trans('A reset link has been sent to your email address.'));
		} else {
			return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
		}
	}

	private function sendResetEmail($email, $token)
	{
		$user = DB::table('users')->where('email', $email)->select('name', 'email')->first();

		$link = config('base_url') . 'password/reset/' . $token . '?email=' . urlencode($user->email);
		try {

			\Mail::to($email)->send(new \App\Mail\ChangePassword($link));

			return true;
		} catch (\Exception $e) {
			return false;
		}
	}

}