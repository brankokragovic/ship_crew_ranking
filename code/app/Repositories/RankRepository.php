<?php

namespace App\Repositories;


use App\Models\Rank;
use App\Repositories\Interfaces\RepositoryInterface;

class RankRepository implements RepositoryInterface
{

	public $rank;

	public function __construct(Rank $rank)
	{
		$this->rank = $rank;
	}

	public function all()
	{
		return $this->rank->all();
	}

	public function getById($id)
	{
		return $this->rank->where('id', $id)->get();
	}

	public function store($request)
	{
		$this->rank->create([
			'name' => $request->name
		]);

		return redirect()->back()->with('success','Rank created');
	}
}