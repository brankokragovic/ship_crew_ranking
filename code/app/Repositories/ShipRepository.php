<?php

namespace App\Repositories;


use App\Models\Ship;
use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Support\Facades\Validator;

class ShipRepository implements RepositoryInterface
{

	public $ship;

	public function __construct(Ship $ship)
	{
		$this->ship = $ship;
	}

	public function all()
	{
		return $this->ship->all();
	}

	public function getById($id)
	{
		return $this->ship->where('id', $id)->get();
	}


	public function store($request)
	{

		if ($request->hasFile('image')) {
			$fileNameExt = $request->file('image')->getClientOriginalName();
			$fileName = pathinfo($fileNameExt, PATHINFO_FILENAME);
			$fileExt = $request->file('image')->getClientOriginalExtension();
			$fileNameToStore = $fileName . '_' . time() . '.' . $fileExt;
			$pathToStore = $request->file('image')->storeAs('public/image', $fileNameToStore);
		}

		$this->ship->create([
			'name' => $request['name'],
			'serial_number' => $request['serial_number'],
			'image_path' => $pathToStore ? $pathToStore : ''
		]);


	}
}