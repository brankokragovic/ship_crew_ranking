<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\RepositoryInterface;
use App\Services\SendEmailService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CrewRepository implements RepositoryInterface
{

	public $user;

	public $sendEmailService;


	public function __construct(User $user, SendEmailService $sendEmailService)
	{
		$this->user = $user;

		$this->sendEmailService = $sendEmailService;

	}

	public function all()
	{
		return $this->user->where('id', '!=', auth()->id())->with('rank')->with('ship')->get();
	}

	public function getById($id)
	{
		return $this->user->where('id', $id)->with('rank')->with('ship')->first();
	}

	public function store($request)
	{
		$this->user->create([
			'name' => $request['name'],
			'surname' => $request['surname'],
			'email' => $request['email'],
			'password' => Hash::make($request['password']),
			'rank_id' => $request['rank_id'],
			'ship_id' => $request['ship_id']
		]);

		$this->sendEmailService->sendEmail($request);

	}

	public function update($request){

		$user = $this->user->where('id',$request->id)->first();
		$user->name = $request->name;
		$user->surname = $request->surname;
		$user->email = $request->email;
		$user->rank_id = $request->rank_id;
		$user->ship_id = $request->ship_id;

		$user->update();

	}

	public function notify($request)
	{
		$users = $this->user->where('rank_id', $request['rank_id'])->get();

		foreach ($users as $user) {
			DB::table('notification')->insert([
				'user_id' => $user->id,
				'notification' => $request['notification'],
			]);
		}
	}
}