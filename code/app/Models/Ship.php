<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
	use HasFactory;

	protected $fillable = [
		'name',
		'serial_number',
		'image_path'
	];

	public function user()
	{
		return $this->hasMany(User::class, 'ship_id');
	}


}
