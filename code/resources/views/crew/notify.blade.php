@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row" style="margin-top: 50px;">
        <div class="col-md-12">
            <div id="showimages"></div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="image-upload" method="post" action="{{ route('memberNotify') }}" enctype="multipart/form-data">
                        @csrf

                        <select class="selectpicker form-control" data-live-search="true" name="rank_id" title="Select Rank">
                            @foreach ($ranks as $rank)
                                <option data-subtext="{{ $rank->name }}"  value="{{ $rank->id }}">{{ $rank->name }}</option>
                            @endforeach
                        </select>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="description" name="notification"></textarea>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success btn-sm">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector:'textarea.description',
        width: 900,
        height: 300
    });
</script>