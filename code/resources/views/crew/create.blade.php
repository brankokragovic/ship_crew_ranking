@extends('layouts.master')

@section('content')
<form method="post" action="{{route('storeMember')}}">
    @csrf()
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
    </div>
    <div class="form-group">
        <label for="surname">Surname</label>
        <input type="text"  name="surname" class="form-control" id="surname" placeholder="Surname" required>
    </div>
    <div class="form-group">
        <label for="Email">Email address</label>
        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" required>
    </div>
    <div class="form-group">
        <label for="Password">Password</label>
        <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
    </div>

    <div class="form-group">
        <label for="rank_id">Select Rank</label>
        <select class="form-control" data-live-search="true" name="rank_id" title="Select Rank">
        @foreach ($ranks as $rank)
            <option data-subtext="{{ $rank->name }}"  value="{{ $rank->id }}">{{ $rank->name }}</option>
        @endforeach
    </select>
    </div>
    <div class="form-group">
        <label for="ship_id">Select Ship</label>
        <select class=" form-control" data-live-search="true" name="ship_id" title="Select Ship">
        @foreach ($ships as $ship)
            <option data-subtext="{{ $ship->name }}"  value="{{ $ship->id }}">{{ $ship->name }}</option>
        @endforeach
    </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection