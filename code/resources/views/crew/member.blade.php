@extends('layouts.master')

@section('content')
    <form method="post" action="{{route('updateMember')}}">
        @csrf()
        <input type="hidden"  name="id" id="id" value="{{$member->id}}">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$member->name}}" placeholder="Name" required>
        </div>
        <div class="form-group">
            <label for="surname">Surname</label>
            <input type="text"  name="surname" class="form-control" id="surname"  value="{{$member->surname}}" placeholder="Surname" required>
        </div>
        <div class="form-group">
            <label for="Email">Email address</label>
            <input type="email" name="email" class="form-control" id="email" value="{{$member->email}}" aria-describedby="emailHelp" placeholder="Enter email" required>
        </div>
        <div class="form-group">
            <label for="rank_id">Select Rank</label>
            <select class="selectpicker"  name="rank_id" title="Select Rank">
                @foreach ($ranks as $rank)
                        <option {{$rank->id == $member->rank_id  ? 'selected' : ''}}  value="{{ $rank->id }}">{{ $rank->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="ship_id">Select Ship</label>
            <select class="selectpicker" data-live-search="true" name="ship_id" title="Select Ship">
            @foreach ($ships as $ship)
                        <option  {{$ship->id == $member->ship_id  ? 'selected' : ''}} value="{{ $ship->id }}">{{ $ship->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection