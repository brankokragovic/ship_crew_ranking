@extends('layouts.master')

@section('content')

    <table class="table table-hover">
        <thead>
        <th>Name</th>
        <th>Surname</th>
        <th>Rank</th>
        <th>Ship</th>
        <th>Action</th>
        </thead>
        <tbody>
        @foreach($data as $member)
            <tr>
                <td>{{$member->name}}</td>
                <td>{{$member->surname}}</td>
                <td>{{$member->rank->name}}</td>
                <td>{{$member->ship->name}}</td>
                <td>
                    <a href='{{ route('crewMember',[$member->id]) }}' class="btn btn-secondary"><i class="mdi mdi-database-edit"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection