@extends('layouts.master')

@section('content')
    <form method="post" action="{{route('storeRank')}}" enctype="multipart/form-data">
        @csrf()
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection