@component('mail::message')
# Introduction

Please change your password.
@component('mail::button', ['url' => $link])
    Change Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
