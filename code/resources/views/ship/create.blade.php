@extends('layouts.master')

@section('content')
    <form method="post" action="{{route('storeShip')}}" enctype="multipart/form-data">
        @csrf()
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name">
        </div>
        <div class="form-group">
            <label for="serial_number">Serial number</label>
            <input type="text"  name="serial_number" class="form-control" id="serial_number" placeholder="Serial number">
        </div>
        <div class="file-field">
            <div class="z-depth-1-half mb-4">
                <img src="https://mdbootstrap.com/img/Photos/Others/placeholder.jpg" class="img-fluid"
                     alt="example placeholder">
            </div>
            <div class="d-flex justify-content-center">
                <div class="btn btn-mdb-color btn-rounded float-left">
                    <span>Choose file</span>
                    <input type="file" name="image">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection