<nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
    <ul class="list-unstyled">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown">
                <i class="menu-icon mdi mdi-human"></i>
                <span class="menu-title">Crew</span>
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{route('createMember')}}">Create new member</a>
                <a class="dropdown-item" href="{{route('showMembers')}}">Show all</a>
                <a class="dropdown-item" href="{{route('createNotify')}}">Send notification</a>
            </div>
        </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown">
            <i class="menu-icon mdi mdi-police-badge"></i>
            <span class="menu-title">Rank</span>
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="{{route('createRank')}}">Create Rank</a>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown">
            <i class="menu-icon mdi mdi-ship-wheel"></i>
            <span class="menu-title">Ship</span>
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="{{route('createShip')}}">Create new ship</a>
        </div>
    </li>
    </ul>
</nav>