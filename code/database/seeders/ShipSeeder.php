<?php

namespace Database\Seeders;

use App\Models\Ship;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ShipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		$ships = [
			[
				'name' => 'Ruby Princess',
				'serial_number' => Str::random(8),
				'image_path' => '/path/',

			],
			[
				'name' => 'Big Bang',
				'serial_number' => Str::random(8),
				'image_path' => '/path/',
			]
		];


		foreach ($ships as $ship){
			Ship::create([

				'name' => $ship['name'],
				'serial_number' => $ship['serial_number'],
				'image_path' => $ship['image_path'],

			]);
		}
    }
}


