<?php

namespace Database\Seeders;

use App\Models\Rank;
use Illuminate\Database\Seeder;

class RankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$ranks = [
			[
				'name' => 'Captain',
			],
			[
				'name' => 'Staff Captain',
			],
			[
				'name' => 'Safety Officer',
			],
			[
				'name' => '1st Officer',
			]
		];


		foreach ($ranks as $rank){
			Rank::create([
				'name' => $rank['name'],
			]);
		}
    }
}
