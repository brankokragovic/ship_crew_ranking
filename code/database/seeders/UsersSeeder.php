<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void */


	public function run() {
		$users = [
			[
				'name' => 'Admin',
				'surname' => 'Admin',
				'email' => 'admin@gmail.com',
				'password' => '123456',
				'rank_id' => '1',
				'ship_id' => '1',
				'is_admin' => '1',
			],
			[
				'name' => 'User',
				'surname' => 'User',
				'email' => 'user@gmail.com',
				'password' => '123456',
				'rank_id' => '2',
				'ship_id' => '1',
				'is_admin' => false,
			]
		];

		foreach($users as $user)
		{
			User::create([
				'name' => $user['name'],
				'surname' => $user['surname'],
				'email' => $user['email'],
				'password' => Hash::make($user['password']),
				'rank_id' => $user['rank_id'],
				'ship_id' => $user['ship_id'],
				'is_admin' => $user['is_admin'],

			]);
		}

	}
}