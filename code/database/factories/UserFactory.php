<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
			'surname' => $this->faker->lastName,
            'email' => $this->faker->email,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi', // password
            'remember_token' => Str::random(10),
			'rank_id' => $this->faker->numberBetween(1,3),
			'ship_id' => $this->faker->numberBetween(1,2)

		];
    }
}
